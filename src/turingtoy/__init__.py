from typing import (
    Dict,
    List,
    Literal,
    Optional,
    Tuple,
    TypedDict,
    Union,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


Movement = Literal["L", "R"]
Action = Literal["write", Movement]
Instruction = Union[Movement, Dict[Action, str]]
TransitionTable = Dict[str, Dict[str, Instruction]]
TuringMachine = TypedDict(
    "TuringMachine",
    {
        "blank": str,
        "start state": str,
        "final states": List[str],
        "table": TransitionTable,
    },
)


class HistoryEntry(TypedDict):
    state: str
    reading: str
    position: int
    memory: str
    transition: Instruction


def run_turing_machine(
    machine: TuringMachine,
    input_str: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[HistoryEntry], bool]:
    # Data extraction
    transition_table = machine["table"]
    final_states = machine["final states"]
    blank_symbol = machine["blank"]

    # Init
    memory: list[str] = list(input_str)
    history: List[HistoryEntry] = []
    state = machine["start state"]
    position = 0
    steps_taken = 0

    while state not in final_states:
        if steps is not None and steps_taken >= steps:
            break

        if position < 0:
            memory.insert(0, blank_symbol)
            position = 0
        elif position >= len(memory):
            memory.append(blank_symbol)

        symbol = memory[position]

        if state not in transition_table or symbol not in transition_table[state]:
            break

        instruction = transition_table[state][symbol]

        history.append(
            HistoryEntry(
                state=state,
                reading=symbol,
                position=position,
                memory="".join(memory),
                transition=instruction,
            )
        )

        if instruction == "L":
            position -= 1
        elif instruction == "R":
            position += 1
        else:
            if "write" in instruction:
                memory[position] = instruction["write"]

            if "L" in instruction:
                position -= 1
                state = instruction["L"]
            elif "R" in instruction:
                position += 1
                state = instruction["R"]
            else:
                position += 0
                break

        steps_taken += 1

    return "".join(memory).strip(blank_symbol), history, state in final_states
